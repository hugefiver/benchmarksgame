# The Computer Language Benchmarks Game

"""
measure with psutil instead of libgtop2
"""
__author__ =  'Isaac Gouy'


from domain import Record

import os, sys, cPickle, time, threading, signal, psutil, resource
from errno import ENOENT
from math import trunc
from subprocess import Popen


def measure(arg,commandline,delay,maxtime,
      outFile=None,errFile=None,inFile=None,logger=None,affinitymask=None):

   r,w = os.pipe()
   forkedPid = os.fork()

   if forkedPid: # read pickled measurements from the pipe
      os.close(w); rPipe = os.fdopen(r); r = cPickle.Unpickler(rPipe)
      measurements = r.load()
      rPipe.close()
      os.waitpid(forkedPid,0)
      return measurements

   else: 
      # Will be destroyed when the forked process _exits
      class TimeoutThread(threading.Thread):

         def __init__(self,processId):
            threading.Thread.__init__(self)
            self.setDaemon(1)
            self.timedout = False 
            self.pid = processId           
            self.start() 
 
         def run(self):
            try: 
               time.sleep(maxtime)       
               self.timedout = True                                 
               os.kill(self.pid, signal.SIGKILL)                               
            except OSError, (e,err):
               if logger: logger.error('%s %s',e,err)         
       
      try:

         m = Record(arg)

         # only write pickles to the pipe
         os.close(r); wPipe = os.fdopen(w, 'w'); w = cPickle.Pickler(wPipe)
         
         psutil.cpu_percent(0,percpu=True) 
         start = time.time()

         # spawn the program in a separate process
         p = Popen(commandline,stdout=outFile,stderr=errFile,stdin=inFile)
         
         t = TimeoutThread( processId = p.pid )          

         # wait for program exit status and resource usage
         rusage = os.wait3(0)
         elapsed = time.time() - start
         load = psutil.cpu_percent(0,percpu=True) 

         # summarize measurements 
         if t.timedout:
            m.setTimedout()
         elif rusage[1] == os.EX_OK:
            m.setOkay()
         else:
            m.setError()

         m.userSysTime = rusage[2][0] + rusage[2][1]    
         m.maxMem = rusage[2][2] 
         m.cpuLoad = ("% ".join([str(trunc(percent)) for percent in load]))+"%"
         m.elapsed = elapsed
         m.notIdle = sum([elapsed*percent/100.0 for percent in load])     

      except KeyboardInterrupt:
         os.kill(p.pid, signal.SIGKILL)

      except ZeroDivisionError, (e,err): 
         if logger: logger.warn('%s %s',err,'too fast to measure?')

      except (OSError,ValueError), (e,err):
         if e == ENOENT: # No such file or directory
            if logger: logger.warn('%s %s',err,commandline)
            m.setMissing() 
         else:
            if logger: logger.error('%s %s',e,err)
            m.setError()       
   
      finally:
         w.dump(m)
         wPipe.close()

         # Sample thread will be destroyed when the forked process _exits
         os._exit(0)

